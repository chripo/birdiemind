#include "arg.h"

int arg_idx(int argc, char **argv, char **keys) {
    int i, key_len;
    const char *key;

    while ((key = *(keys++))) {
        key_len = strlen(key);
        for (i = 0; i < argc && strncmp(argv[i], key, key_len); i++);
        if (i < argc)
            return i;
    }
    return argc + 1;
}

int arg_int(int argc, char **argv, int def, char **keys) {
    int idx = arg_idx(argc, argv, keys);
    return idx < argc-1 ? strtol(argv[idx+1], NULL, 10): def;
}

const char* arg_str(int argc, char **argv, const char *def, char **keys) {
    int idx = arg_idx(argc, argv, keys);
    return idx < argc-1 ? argv[idx+1] : def;
}

int arg_isset(int argc, char **argv, char **keys) {
    return arg_idx(argc, argv, keys) < argc ? 1 : 0;
}
