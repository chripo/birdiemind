/* See LICENSE file for copyright and license details.
 * birthday reminder
 * www.christoph-polcin.com
 */

#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wchar.h>

#include "arg.h"
#include "xmalloc.h"

#define STR(x) STR2(x)
#define STR2(x) #x

#define MAX_STR_LEN 254

struct BirthDay {
    int y;
    int m;
    int d;
    wchar_t name[MAX_STR_LEN+1];
};

char* resolve_path(const char *v) { /*{{{*/
    const char *prefix = "";

    if (*v == '~') {
        v++;
        prefix = getenv("HOME");
    }
    size_t len = strlen(prefix) + strlen(v) + 1;
    char *path = xmalloc(len);

    if ( (len - 1) != snprintf(path, len, "%s%s", prefix, v) ) {
        xfree(path);
        return NULL;
    }

    return path;
} /*}}}*/

time_t mk_date(int day, int month) { /*{{{*/
    time_t now = time(NULL);
    struct tm *tm_now = localtime(&now);
    tm_now->tm_sec = tm_now->tm_min = tm_now->tm_hour = 0;

    if (day > 0)
        tm_now->tm_mday = day;
    if (month > 0)
        tm_now->tm_mon = month - 1;

    return mktime(tm_now);
} /*}}}*/

int main(int argc, char **argv) {

    setlocale(LC_ALL, "");

    if (argc > 1 && arg_is(argc, argv, "-h", "-help", "--help")) {
        fprintf(stderr, "usage: birdiemind [-b|s <0>] [-e <7>] [-db <path>] [-h] [Name]\n");
        fprintf(stderr, "visit: http://git.christoph-polcin.com/birdiemind/\n");
        return EXIT_FAILURE;
    }

    char *db_path = resolve_path(arg_s(argc, argv, "~/.config/birdiemind.csv", "-db"));

    FILE *db = fopen(db_path, "r");
    if (!db) {
        fprintf(stderr, "fail to open database: '%s' [MM,DD,YYYY,NAME,]\n", db_path);
        return EXIT_FAILURE;
    }

    free(db_path);

    const int begin = arg_i(argc, argv, 0, "-b", "-s", "-begin", "-start");
    const int end = arg_i(argc, argv, 7, "-e", "-end");

    const time_t today_t = mk_date(0, 0);
    time_t day_t;
    float dif_day;
    char tbuffer[MAX_STR_LEN + 1];
    struct BirthDay b;
    struct tm *day_tm;
    wchar_t *filter = argc >= 2 && !arg_is(argc, argv, \
            "-h", "-help", "--help", "-db", \
            "-b", "-s", "-begin", "-start", \
            "-e", "-end") ? xctowc(argv[1]) : NULL;

    tbuffer[0] = 0;
    if (filter)
        fprintf(stdout, "Filter by: %ls\n\n", filter);
    else
        fprintf(stdout, "Upcoming birthdays:\n\n");
    while (fscanf(db, "%2d,%2d,%4i,%"STR(MAX_STR_LEN)"l[^,],\n", &b.m, &b.d, &b.y, b.name) == 4) {
        day_t = mk_date(b.d, b.m);
        dif_day = difftime(day_t, today_t) / 60 / 60 / 24;

        if ( (filter && wcsstr(b.name, filter)) || (!filter && dif_day >= begin && dif_day <= end) ) {
            day_tm = localtime(&day_t);
            strftime(tbuffer, MAX_STR_LEN, "(%a, %d. %b)", day_tm);
            fprintf(stdout, " %ls [%i] %s, %.0f day%s %s.\n",
                        b.name,
                        (1900 + day_tm->tm_year - b.y),
                        tbuffer,
                        dif_day < 0 ? dif_day * -1 : dif_day,
                        dif_day == 1 ? "" : "s",
                        dif_day < 0 ? "ago" : "left");
        }
    }

    fclose(db);

    xfree(filter);

    if (*tbuffer)
        fprintf(stdout, "\n");

    return EXIT_SUCCESS;
}

/* vim: set expandtab fdm=marker fen ts=4 sw=4: */

