#ifndef ARG_H
#define ARG_H

/**
 * @author christoph polcin
 */

#include <stdlib.h>
#include <string.h>

#if (__STDC_VERSION__ >= 199901L)

#define arg_i(argc, argv, def, ...) \
        arg_int(argc, argv, def, (char *[]){__VA_ARGS__, NULL})

#define arg_s(argc, argv, def, ...) \
        arg_str(argc, argv, def, (char *[]){__VA_ARGS__, NULL})

#define arg_is(argc, argv, ...) \
        arg_isset(argc, argv, (char *[]){__VA_ARGS__, NULL})
#endif

int arg_idx(int argc, char **argv, char **keys);

int arg_int(int argc, char **argv, int def, char **keys);

const char* arg_str(int argc, char **argv, const char *def, char **keys);


int arg_isset(int argc, char **argv, char **keys);

#endif
