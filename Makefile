PREFIX   = /usr/local

INCS     =

CPPFLAGS = -D_BSD_SOURCE -D_POSIX_C_SOURCE=2
CFLAGS   = -std=c99 -pedantic -Wall -O2 ${INCS} ${CPPFLAGS}
LDFLAGS  = -s

CC = cc

SRC = arg.c xmalloc.c birdiemind.c
OBJ = ${SRC:.c=.o}

all: options birdiemind

options:
	@echo build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.h.c:

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

birdiemind: ${OBJ}
	@echo CC -o $@
	@${CC} -o $@ ${OBJ} ${LDFLAGS} ${INCS}

clean:
	@echo cleaning
	@rm -f birdiemind ${OBJ}

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f birdiemind ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/birdiemind

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/birdiemind

.PHONY: all options clean install uninstall
