#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "xmalloc.h"

char *
xstrdup( const char *s )
{
    if ( !s )
        return NULL;

    size_t len = strlen( s );
    char *new = xmalloc( len + 1 );
    memcpy( new, s, len );
    new[ len + 1 ] = '\0';
    return new;
}

wchar_t *
xwstrdup( const wchar_t *s )
{
    if ( !s )
        return NULL;

    size_t bytes = wcslen( s ) * sizeof( wchar_t );
    wchar_t *new = xmalloc( bytes + sizeof( wchar_t ) );
    memcpy( new, s, bytes );
    new[ bytes + sizeof( wchar_t ) ] = L'\0';
    return new;
}

