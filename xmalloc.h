#ifndef _XMALLOC_H
#define _XMALLOC_H 1

extern void* xmalloc( size_t n );
extern void* xcalloc( size_t nblocks, size_t size );

extern void xfree( void *ptr );

extern char* xstrdup( const char *s );
extern wchar_t * xwstrdup (const wchar_t *s );

extern wchar_t* xctowc( const char *s );

#endif
