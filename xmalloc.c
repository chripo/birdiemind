#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "xmalloc.h"

#define errmsg "libcutils: out of memory [%zu bytes]\n"

void*
xmalloc( size_t n )
{
    void *p = malloc(n);
    if ( !p ) {
        fprintf( stderr, errmsg , n );
        exit( EXIT_FAILURE );
    }
    return p;
}

void*
xcalloc( size_t nblocks, size_t size )
{
    void *p = NULL;

    if ( size == 0 || nblocks == 0 || ((SIZE_MAX / nblocks) < size) ) {
        fprintf( stderr, "xcalloc: argument error.\n" );
        exit( EXIT_FAILURE );
    }

    p = calloc( nblocks, size );
    if ( !p ) {
        fprintf( stderr, errmsg, (size * nblocks) );
        exit( EXIT_FAILURE );
    }
    return p;
}

void
xfree( void *ptr )
{
    if ( ptr )
        free( ptr );
}

wchar_t*
xctowc( const char *s ) {
    if ( !s )
        return NULL;

    size_t l = mbstowcs( NULL, s, 0 );
    wchar_t *p = xmalloc( (l + 1) * sizeof(wchar_t) );
    mbstowcs(p, s, l);
    p[l] = L'\0';

    return p;
}
